package com.glitchcube;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

/**
 * Created by Frank Navarsete on 18.10.2015.
 */
@Stateless
@Path("book")
public class BookEngine {

    @PersistenceContext
    private EntityManager em;

    @GET
    @Produces("application/json")
    public List<Book> newBook() {
        Book book = new Book("my first book");
        Book merge = em.merge(book);

        List<Book> resultList = em.createQuery("select b from Book b", Book.class).getResultList();
        return resultList;
    }
}
