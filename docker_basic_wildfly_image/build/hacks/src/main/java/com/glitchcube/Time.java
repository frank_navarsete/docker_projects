package com.glitchcube;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.time.LocalDateTime;

/**
 * Created by Frank Navarsete on 17.10.2015.
 */
@Path("time")
public class Time {

    @GET
    public String currentTime() {
        System.out.println("hello!!!!");
        return LocalDateTime.now().toString();
    }
}
