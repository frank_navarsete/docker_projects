package com.glitchcube;

import javax.persistence.*;

/**
 * Created by Frank Navarsete on 18.10.2015.
 */
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Column
    private String name;

    public Book(String name) {
        this.name = name;
    }

    public Book() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
